all: test Makefile

test: main.o chan.o g.o Makefile
	${CC} -Wl,-z,defs -o test main.o chan.o g.o -lpthread

%.o: %.c chan.h chan_int.h chan_common.h g.h tests.h Makefile
	${CC} --std=gnu11 -Ofast -DNO_CRASH_ON_PANIC -fmax-errors=2 -Wall -Wextra -Werror -o $@ -c $<


run: all Makefile
	./test

clean:
	-rm *.o test

.PHONY: clean all run
