#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>

#include "tests.h"
#include "chan.h"
#include "chan_int.h"

/* tags:
*	// TODO:
*	// MAYBE:
*	// PTHREAD_RETURN:
*	// DIFFERENCE:
*/

/* tests:
*	(x) Basic functionality 1: create unbuffered channel
*	(x) Basic functionality 2: create buffered channel
*	(x) Basic functionality 3: nonblocking read with no send returns !ok
*	(x) Basic functionality 4: nonblocking write with no read returns !ok
*	(x) Basic functionality 5: nonblocking read with send
*	(x) Basic functionality 6: nonblocking write with recv
*	(x) Basic functionality 7: unbuffered send/recv a NULL as an edge case
*	(x) Basic functionality 8: buffered send/recv a NULL as an edge case
*
*	(x) Semantics 1: Write to unbuffered channel with no readers blocks
*	(x) Semantics 2: Write to full buffered channel blocks
*	(x) Semantics 3: Write to channel with blocked reader succeeds
*	(x) Semantics 4: Write to buffered channel with room succeeds
*	(x) Semantics 5: Write to closed channel panics
*	(x) Semantics 6: Write to NULL channel blocks forever
*	(x) Semantics 7: Blocked write on channel, which is then closed, panics
*	(x) Semantics 8: Read from unbuffered channel with no writers blocks
*	(x) Semantics 9: Read from empty buffered channel blocks
*	(x) Semantics 10: Read from channel with blocked writer succeeds
*	(x) Semantics 11: Read from buffered channel with data succeeds
*	(x) Semantics 12: Read from empty or unbuffered closed channel returns !ok
*	(x) Semantics 13: Read from closed buffered channel with data succeeds
*	(x) Semantics 14: Blocked read on channel, which is then closed, returns !ok
*	(x) Semantics 15: Read from NULL channel blocks forever
*	(x) Semantics 16: Closing a closed channel panics
*	(x) Semantics 17: Closing a null channel panics
*	(x) Semantics 18: Select on closed recv channel returns immediately with !ok
*	(x) Semantics 19: Select on NULL channel blocks forever
*	(x) Semantics 20: Select on closed send channel panics
*
*	(x) Select 1: non blocking select read without data returns !ok
*	(x) Select 2: non blocking select write without room returns !ok
*	(x) Select 3: non blocking buffered select read with data succeeds
*	(x) Select 4: non blocking buffered select write with room succeeds
*	(x) Select 5: non blocking unbuffered select read with blocked writer succeeds
*	(x) Select 6: non blocking unbuffered select write with blocked reader succeeds
*	(x) Select 7: blocking buffered select read with data succeeds
*	(x) Select 8: blocking buffered select write with room succeeds
*	(x) Select 9: blocking unbuffered select read with blocked writer succeeds
*	(x) Select 10: blocking unbuffered select write with blocked reader succeeds
*	(x) Select 11: select multiple reads succeeds on correct channel
*	(x) Select 12: select multiple writes succeeds on correct channel
*	(x) Select 13: select read, write succeeds on correct channel
*	(x) Select 14: select multiple reads, writes succeeds on correct channel
*	(x) Select 15: select read timeout with timer and no data times out.
*
*	(x) End-to-end Basic functionality 1: buffered nonblocking write and readback
*	(x) End-to-end Basic functionality 2: blocked select reader until write happens succeeds
*/

int fails = 0;
int main()
{
	int sz;
	struct chan* ch;
	bool ok;
	bool selected;
	void* in;
	void* out;
	pthread_t tid;



	TEST("Basic functionality 1: create unbuffered channel", 0)
		for (int i = 0; i < sz; i++)
		{
			printf("buf[%d]: %p\n", i, ch->buf[i]);
			testErr("failed creating zero size channel");
		}
	ENDTEST();



	TEST("Basic functionality 2: create buffered channel", 3)
		int i;
		for (i = 0; i < sz; i++)
		{
			if(i > 2)
			{
				printf("buf[%d]: %p\n", i, ch->buf[i]);
				testErr("failed creating size 3 channel");
			}
		}
		if(i != 3)
			testErr("failed creating size 3 channel");
	ENDTEST();



	TEST("Semantics 16: Closing a closed channel panics", 0)
		closechan(ch);
		closechan(ch);
		if(chan_crashed != 1)
			testErr("close of closed channel test failed because it didn't panic");
	ENDTEST();



	TEST("Semantics 4: Write to buffered channel with room succeeds\n"
		"\tSemantics 11: Read from buffered channel with data succeeds", 2)
		ok = sendchan(ch, in, BLOCK);
		if(!ok)
			testErr("blocking buffered send with room failed");
		ok = recvchan(ch, &out, BLOCK, &selected);
		if(!ok)
			testErr("blocking buffered recv with data failed");
		if(in != out)
		{
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
			testErr("blocking buffered send then recieve failed");
		}
		// TODO: check selected
	ENDTEST();



	TEST("Semantics 8: Read from unbuffered channel with no writers blocks\n"
		"\tSemantics 3: Write to channel with blocked reader succeeds", 0)
		pthread_create(&tid, NULL, delayed_blocking_send, ch);
		ok = recvchan(ch, &out, BLOCK, &selected);
		if(!ok)
			testErr("blocking unbuffered recv (with send after delay) failed");
		pthread_join(tid, &in);
		if(!in)
			testErr("delayed blocking send failed");
		if(in != out)
		{
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
		}
		// TODO: check selected
	ENDTEST();



	TEST("Semantics 1: Write to unbuffered channel with no readers blocks\n"
		"\tSemantics 10: Read from channel with blocked writer succeeds", 0)
		// test blocking send then recv on unbuffered channel
		pthread_create(&tid, NULL, delayed_blocking_recv, ch);
		ok = sendchan(ch, in, BLOCK);
		if(!ok)
			testErr("blocking unbuffered send (with recv after delay) failed");
		pthread_join(tid, &out);
		if(!out)
			testErr("delayed blocking recv failed");
		if(in != out)
		{
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
		}
	ENDTEST();



	TEST("Basic functionality 3: nonblocking read with no send returns !ok", 0)
		ok = recvchan(ch, &out, NONBLOCK, &selected);
		if(ok)
			testErr("nonblocking unbuffered recv (with no send) claimed ok");
		if(out != NULL)
			testErr("failed nonblocking unbuffered recv set out");
		// TODO: check selected
	ENDTEST();



	TEST("Basic functionality 4: nonblocking write with no read returns !ok", 0)
		ok = sendchan(ch, in, NONBLOCK);
		if(ok)
			testErr("nonblocking unbuffered send (with no recv) claimed ok");
	ENDTEST();



	TEST("End-to-end Basic functionality 1: buffered nonblocking write and readback", 1)
		ok = sendchan(ch, in, NONBLOCK);
		if(!ok)
			testErr("nonblocking buffered send (with room) failed");
		ok = sendchan(ch, (void*)((uintptr_t)(in)+1), NONBLOCK);
		if(ok)
			testErr("nonblocking buffered send (without room) claimed ok");
		ok = recvchan(ch, &out, NONBLOCK, &selected);
		if(!ok)
			testErr("nonblocking buffered recv (with data) failed");
		ok = recvchan(ch, &out, NONBLOCK, &selected);
		if(ok)
			testErr("nonblocking unbuffered recv (with no data) claimed ok");
		if(in != out)
		{
			testErr("failed nonblocking unbuffered recv set out");
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
		}
		// TODO: check all selected
	ENDTEST();



	TEST("Semantics 2: Write to full buffered channel blocks", 2)
		ok = sendchan(ch, in, BLOCK);
		if(!ok)
			testErr("blocking buffered send (with room) failed");
		ok = sendchan(ch, in, BLOCK);
		if(!ok)
			testErr("blocking buffered send (with room) failed");
		pthread_create(&tid, NULL, delayed_blocking_recv, ch);
		ok = sendchan(ch, in, BLOCK);
		if(!ok)
			testErr("blocking buffered send (with recv after delay) failed");
		pthread_join(tid, &out);
		if(!out)
			testErr("delayed blocking recv failed");
	ENDTEST();



	TEST("Semantics 5: Write to closed channel panics", 2)
		closechan(ch);
		ok = sendchan(ch, in, BLOCK);
		if(chan_crashed != 1)
			testErr("write to closed channel failed because it didn't panic");
	ENDTEST();



	pthread_create(&tid, NULL, test_writeNull, NULL);
	usleep(LONGDELAY);
	pthread_detach(tid);



	TEST("Semantics 7: Blocked write on channel, which is then closed, panics", 0)
		pthread_create(&tid, NULL, delayed_close, ch);
		ok = sendchan(ch, in, BLOCK);
		if(chan_crashed != 1)
			testErr("write to closed channel failed because it didn't panic");
	ENDTEST();



	TEST("Semantics 9: Read from empty buffered channel blocks", 2)
		pthread_create(&tid, NULL, delayed_blocking_send, ch);
		ok = recvchan(ch, &out, BLOCK, &selected);
		if(!ok)
			testErr("blocking buffered recv (with send after delay) failed");
		pthread_join(tid, &in);
		if(!in)
			testErr("delayed blocking send failed");
		if(in != out)
		{
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
		}
		// TODO: check selected
	ENDTEST();



	TEST("Semantics 12: Read from empty or unbuffered closed channel returns !ok", 0)
		closechan(ch);
		ok = recvchan(ch, &out, BLOCK, &selected);
		if(ok)
			testErr("read empty closed channel claimed ok");
		// TODO: check selected
	ENDTEST();



	TEST("Semantics 13: Read from closed buffered channel with data succeeds", 3)
		sendchan(ch, in, BLOCK);
		sendchan(ch, (void*)((uintptr_t)(in)+1), BLOCK);
		closechan(ch);
		ok = recvchan(ch, &out, BLOCK, &selected);
		if(in != out)
		{
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
		}
		// TODO: check selected
	ENDTEST();



	TEST("Semantics 14: Blocked read on channel, which is then closed, returns !ok", 0)
		pthread_create(&tid, NULL, delayed_close, ch);
		ok = recvchan(ch, &out, BLOCK, &selected);
		if(ok)
			testErr("read closed channel claimed ok");
		pthread_join(tid, &in);
		// TODO: check selected
	ENDTEST();



	pthread_create(&tid, NULL, test_readNull, NULL);
	usleep(LONGDELAY);
	pthread_detach(tid);



	TEST("Semantics 17: Closing a null channel panics", 0)
		closechan(NULL);
		if(chan_crashed != 1)
			testErr("close of NULL channel test failed because it didn't panic");
	ENDTEST();



	TEST("Basic functionality 5: nonblocking read with send", 0)
		pthread_create(&tid, NULL, blocking_send, ch);
		usleep(DELAY);
		ok = recvchan(ch, &out, NONBLOCK, &selected);
		if(!ok)
			testErr("nonblocking unbuffered recv (with blocked send) failed");
		pthread_join(tid, &in);
		if(in != out)
		{
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
		}
		// TODO: check selected
	ENDTEST();



	TEST("Basic functionality 6: nonblocking write with recv", 0)
		pthread_create(&tid, NULL, blocking_recv, ch);
		usleep(DELAY);
		ok = sendchan(ch, in, NONBLOCK);
		if(!ok)
			testErr("nonblocking unbuffered send (with blocked recv) failed");
		pthread_join(tid, &out);
		if(in != out)
		{
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
		}
	ENDTEST();



	TEST("Basic functionality 7: unbuffered send/recv a NULL as an edge case", 0)
		out = (void*)1;
		pthread_create(&tid, NULL, blocking_send_NULL, ch);
		ok = recvchan(ch, &out, BLOCK, &selected);
		if(!ok)
			testErr("blocking unbuffered recv (with blocked send) failed");
		pthread_join(tid, &in);
		if(in != out)
		{
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
		}
		// TODO: check selected
	ENDTEST();



	TEST("Basic functionality 8: buffered send/recv a NULL as an edge case", 2)
		out = (void*)1;
		in = NULL;
		ok = sendchan(ch, in, BLOCK);
		if(!ok)
			testErr("blocking buffered send with room failed");
		ok = recvchan(ch, &out, BLOCK, &selected);
		if(!ok)
			testErr("blocking buffered recv with data failed");
		if(in != out)
		{
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
			testErr("blocking buffered send then recieve failed");
		}
		// TODO: check selected
	ENDTEST();



	TEST("Semantics 18: Select on closed recv channel returns immediately with !ok", 0)
		closechan(ch);
		selected = selectchan(&ch, 1, &out, &ok, NULL, 0, NULL, BLOCK);
		if(selected != 0)
		{
			testErr("wrong case selected");
		}
		if(ok)
		{
			testErr("should have returned !ok");
		}
	ENDTEST();



	pthread_create(&tid, NULL, test_selectNull, NULL);
	usleep(LONGDELAY);
	pthread_detach(tid);



	TEST("Semantics 20: Select on closed send channel panics", 0)
		closechan(ch);
		selected = selectchan(NULL, 0, NULL, &ok, &ch, 1, &in, BLOCK);
		if(chan_crashed != 1)
			testErr("select of closed send channel test failed because it didn't panic");
	ENDTEST();



	TEST("Select 1: non blocking select read without data returns !ok", 0)
		selected = selectchan(&ch, 1, &out, &ok, NULL, 0, NULL, NONBLOCK);
		if(selected != 1)
		{
			testErr("wrong case selected");
		}
		if(ok)
		{
			testErr("should have returned !ok");
		}
	ENDTEST();



	TEST("Select 2: non blocking select write without room returns !ok", 0)
		selected = selectchan(NULL, 0, NULL, &ok, &ch, 1, &in, NONBLOCK);
		if(selected != 1)
		{
			testErr("wrong case selected");
		}
		if(ok)
		{
			testErr("should have returned !ok");
		}
	ENDTEST();



	TEST("Select 3: non blocking buffered select read with data succeeds", 2)
		sendchan(ch, in, BLOCK);
		selected = selectchan(&ch, 1, &out, &ok, NULL, 0, NULL, NONBLOCK);
		if(selected != 0)
		{
			testErr("wrong case selected");
		}
		if(!ok)
		{
			testErr("should have returned ok");
		}
		if(in != out)
		{
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
		}
	ENDTEST();



	TEST("Select 4: non blocking buffered select write with room succeeds", 2)
		selected = selectchan(NULL, 0, NULL, &ok, &ch, 1, &in, NONBLOCK);
		if(selected != 0)
		{
			testErr("wrong case selected");
		}
		if(!ok)
		{
			testErr("should have returned ok");
		}
		ok = recvchan(ch, &out, BLOCK, &selected);
		if(in != out)
		{
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
		}
	ENDTEST();



	TEST("Select 5: non blocking unbuffered select read with blocked writer succeeds", 0)
		pthread_create(&tid, NULL, blocking_send, ch);
		usleep(DELAY);
		selected = selectchan(&ch, 1, &out, &ok, NULL, 0, NULL, NONBLOCK);
		if(selected != 0)
		{
			testErr("wrong case selected");
		}
		if(!ok)
		{
			testErr("should have returned ok");
		}
		pthread_join(tid, &in);
		if(in != out)
		{
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
		}
	ENDTEST();



	TEST("Select 6: non blocking unbuffered select write with blocked reader succeeds", 0)
		pthread_create(&tid, NULL, blocking_recv, ch);
		usleep(DELAY);
		selected = selectchan(NULL, 0, NULL, &ok, &ch, 1, &in, NONBLOCK);
		if(selected != 0)
		{
			testErr("wrong case selected");
		}
		if(!ok)
		{
			testErr("should have returned ok");
		}
		pthread_join(tid, &out);
		if(in != out)
		{
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
		}
	ENDTEST();



	TEST("Select 7: blocking buffered select read with data succeeds", 2)
		sendchan(ch, in, BLOCK);
		selected = selectchan(&ch, 1, &out, &ok, NULL, 0, NULL, BLOCK);
		if(selected != 0)
		{
			testErr("wrong case selected");
		}
		if(!ok)
		{
			testErr("should have returned ok");
		}
		if(in != out)
		{
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
		}
	ENDTEST();



	TEST("Select 8: blocking buffered select write with room succeeds", 2)
		selected = selectchan(NULL, 0, NULL, &ok, &ch, 1, &in, BLOCK);
		if(selected != 0)
		{
			testErr("wrong case selected");
		}
		if(!ok)
		{
			testErr("should have returned ok");
		}
		ok = recvchan(ch, &out, BLOCK, &selected);
		if(in != out)
		{
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
		}
	ENDTEST();



	TEST("Select 9: blocking unbuffered select read with blocked writer succeeds", 0)
		pthread_create(&tid, NULL, blocking_send, ch);
		usleep(DELAY);
		selected = selectchan(&ch, 1, &out, &ok, NULL, 0, NULL, BLOCK);
		if(selected != 0)
		{
			testErr("wrong case selected");
		}
		if(!ok)
		{
			testErr("should have returned ok");
		}
		pthread_join(tid, &in);
		if(in != out)
		{
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
		}
	ENDTEST();



	TEST("Select 10: blocking unbuffered select write with blocked reader succeeds", 0)
		pthread_create(&tid, NULL, blocking_recv, ch);
		usleep(DELAY);
		selected = selectchan(NULL, 0, NULL, &ok, &ch, 1, &in, BLOCK);
		if(selected != 0)
		{
			testErr("wrong case selected");
		}
		if(!ok)
		{
			testErr("should have returned ok");
		}
		pthread_join(tid, &out);
		if(in != out)
		{
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
		}
	ENDTEST();



	TEST("End-to-end Basic functionality 2: blocked select reader until write happens succeeds", 0)
		pthread_create(&tid, NULL, delayed_blocking_send, ch);
		selected = selectchan(&ch, 1, &out, &ok, NULL, 0, NULL, BLOCK);
		if(selected != 0)
		{
			testErr("wrong case selected");
		}
		if(!ok)
		{
			testErr("should have returned ok");
		}
		pthread_join(tid, &in);
		if(in != out)
		{
			printf("in: %p, out: %p\n", in, out);
			testErr("in != out");
		}
	ENDTEST();



	TESTmanual("Select 11: select multiple reads succeeds on correct channel")
		struct chan* ch1 = makechan(0);
		struct chan* ch2 = makechan(0);

		pthread_create(&tid, NULL, delayed_blocking_send, ch2);

		struct chan* rds[] = {ch1, ch2};
		int bad = 0;
		switch(selectchan(rds, 2, &out, &ok, NULL, 0, NULL, BLOCK))
		{
			case 0: // ch1
				bad = 1;
				testErr("wrong case");
				break;
			case 1: // ch2
				pthread_join(tid, &in);
				if(in != out)
				{
					bad = 1;
					printf("in: %p, out: %p\n", in, out);
					testErr("in != out");
				}
				break;
			default:
				bad = 1;
				testErr("impossible return?");
				break;
		}
		if(bad)
		{
			pthread_detach(tid);
			testErr("bad select");
		}

		destroychan(ch1);
		destroychan(ch2);
	ENDTESTmanual();



	TESTmanual("Select 12: select multiple writes succeeds on correct channel")
		struct chan* ch1 = makechan(0);
		struct chan* ch2 = makechan(0);

		pthread_create(&tid, NULL, delayed_blocking_recv, ch2);

		struct chan* wrs[] = {ch1, ch2};
		void* wrvals[] = {(void*)((uintptr_t)(in)+1), in};
		int bad = 0;
		switch(selectchan(NULL, 0, NULL, &ok, wrs, 2, wrvals, BLOCK))
		{
			case 0: // ch1
				bad = 1;
				testErr("wrong case");
				break;
			case 1: // ch2
				pthread_join(tid, &out);
				if(in != out)
				{
					bad = 1;
					printf("in: %p, out: %p\n", in, out);
					testErr("in != out");
				}
				break;
			default:
				bad = 1;
				testErr("impossible return?");
				break;
		}
		if(bad)
		{
			pthread_detach(tid);
			testErr("bad select");
		}

		destroychan(ch1);
		destroychan(ch2);
	ENDTESTmanual();



	TESTmanual("Select 13: select read, write succeeds on correct channel")
		struct chan* ch1 = makechan(0);
		struct chan* ch2 = makechan(0);

		pthread_create(&tid, NULL, delayed_blocking_recv, ch2);

		int bad = 0;
		switch(selectchan(&ch1, 1, &out, &ok, &ch2, 1, &in, BLOCK))
		{
			case 0: // ch1
				bad = 1;
				testErr("wrong case");
				break;
			case 1: // ch2
				pthread_join(tid, &out);
				if(in != out)
				{
					bad = 1;
					printf("in: %p, out: %p\n", in, out);
					testErr("in != out");
				}
				break;
			default:
				bad = 1;
				testErr("impossible return?");
				break;
		}
		if(bad)
		{
			pthread_detach(tid);
			testErr("bad select");
		}

		destroychan(ch1);
		destroychan(ch2);
	ENDTESTmanual();



	TESTmanual("Select 14: select multiple reads, writes succeeds on correct channel")
		struct chan* ch1 = makechan(0);
		struct chan* ch2 = makechan(0);
		struct chan* ch3 = makechan(0);
		struct chan* ch4 = makechan(0);

		pthread_create(&tid, NULL, delayed_blocking_recv, ch4);

		struct chan* rds[] = {ch1, ch2};
		struct chan* wrs[] = {ch3, ch4};
		void* wrvals[] = {(void*)((uintptr_t)(in)+1), in};
		int bad = 0;
		switch(selectchan(rds, 2, &out, &ok, wrs, 2, wrvals, BLOCK))
		{
			case 0: // ch1
			case 1: // ch2
			case 2: // ch3
				bad = 1;
				testErr("wrong case");
				break;
			case 3: // ch4
				pthread_join(tid, &out);
				if(in != out)
				{
					bad = 1;
					printf("in: %p, out: %p\n", in, out);
					testErr("in != out");
				}
				break;
			default:
				bad = 1;
				testErr("impossible return?");
				break;
		}
		if(bad)
		{
			pthread_detach(tid);
			testErr("bad select");
		}
		if(chan_crashed == 1)
		{
			testErr("channel crashed when it shouldn't");
		}

		destroychan(ch1);
		destroychan(ch2);
		destroychan(ch3);
		destroychan(ch4);
	ENDTESTmanual();



	TESTmanual("Select 15: select read timeout with timer and no data times out.")
		struct chan* ch1 = makechan(0);
		struct chan* timech = makechan(0);

		pthread_create(&tid, NULL, delayed_blocking_send, timech);

		struct chan* rds[] = {ch1, timech};
		int bad = 0;
		switch(selectchan(rds, 2, &out, &ok, NULL, 0, NULL, BLOCK))
		{
			case 0: // ch1
				bad = 1;
				testErr("wrong case");
				break;
			case 1: // timech
				pthread_join(tid, &in);
				if(in != out)
				{
					bad = 1;
					printf("in: %p, out: %p\n", in, out);
					testErr("in != out");
				}
				break;
			default:
				bad = 1;
				testErr("impossible return?");
				break;
		}
		if(bad)
		{
			pthread_detach(tid);
			testErr("bad select");
		}

		destroychan(ch1);
		destroychan(timech);
	ENDTESTmanual();



	if(fails == 0)
		printf("All tests passed.\n");
	else if(fails > 1)
		printf("%d tests failed.\n", fails);
	else
		printf("%d test failed.\n", fails);
	return fails;
}

void* delayed_blocking_send(void* c)
{
	bool ok;
	struct chan* ch = c;
	void* in = rand();

	usleep(DELAY);
	ok = sendchan(ch, in, BLOCK);
	if(!ok)
	{
		return NULL;
	}

	return in;
}

void* delayed_blocking_recv(void* c)
{
	bool ok;
	void* out;
	bool selected;
	struct chan* ch = c;

	usleep(DELAY);
	ok = recvchan(ch, &out, BLOCK, &selected);
	if(!ok)
	{
		return NULL;
	}

	return out;
}

void* blocking_send(void* c)
{
	bool ok;
	struct chan* ch = c;
	void* in = rand();

	ok = sendchan(ch, in, BLOCK);
	if(!ok)
	{
		return NULL;
	}

	return in;
}

void* blocking_recv(void* c)
{
	bool ok;
	void* out;
	bool selected;
	struct chan* ch = c;

	ok = recvchan(ch, &out, BLOCK, &selected);
	if(!ok)
	{
		return NULL;
	}

	return out;
}

void* blocking_send_NULL(void* c)
{
	bool ok;
	struct chan* ch = c;
	void* in = NULL;

	ok = sendchan(ch, in, BLOCK);
	if(!ok)
	{
		return NULL;
	}

	return in;
}

void* delayed_close(void* c)
{
	struct chan* ch = c;

	usleep(DELAY);
	closechan(ch);

	return NULL;
}

void* test_writeNull(void* c)
{
	int sz;
	struct chan* ch;
	bool ok;
	void* in;
	void* out;

	TEST("Semantics 6: Write to NULL channel blocks forever", 0)
		ok = sendchan(c, in, BLOCK);
		testErr("write to NULL channel failed because it returned");
	ENDTEST();

	(void) out;
	(void) ok;
	return NULL;
}

void* test_readNull(void* c)
{
	int sz;
	struct chan* ch;
	bool ok;
	void* in;
	void* out;

	TEST("Semantics 15: Read from NULL channel blocks forever", 0)
		ok = sendchan(c, in, BLOCK);
		testErr("read from NULL channel failed because it returned");
	ENDTEST();

	(void) out;
	(void) ok;
	return NULL;
}

void* test_selectNull(void* c)
{
	int sz;
	int selected;
	struct chan* ch;
	struct chan* chNULL = NULL;
	bool ok;
	void* in;
	void* out;

	TEST("Semantics 19: Select on NULL channel blocks forever", 0)
		selected = selectchan(&chNULL, 1, &out, &ok, NULL, 0, NULL, BLOCK);
		testErr("select with NULL channel failed because it returned");
	ENDTEST();

	(void) out;
	(void) ok;
	(void) in;
	(void) c;
	(void) selected;
	return NULL;
}

void* rand()
{
	void* ret;

	FILE* fp = fopen("/dev/urandom", "r");
	if(!fp)
	{
		return (void*)5;
	}
	int n = fread(&ret, sizeof(ret), 1, fp);
	fclose(fp);

	if(n != 1)
	{
		return (void*)6;
	}
	if(ret == NULL)
	{
		return (void*)7;
	}
	return ret;
}
