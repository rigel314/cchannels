#ifndef _CHAN_H
#define _CHAN_H

#include <stdbool.h>

#define BLOCK 1
#define NONBLOCK 0

// Opaque structure for channels
struct chan;

// Create a new channel, returns NULL on error.
struct chan* makechan(int size);

// Destroy an existing channel, this will close the channel before trying to free resources.  Not safe to call more than once on a channel, or on an uninitialized channel
void destroychan(struct chan* c);

// Close a channel, causes blocked readers to return unsuccessful, blocked writers to panic, but buffered channels retain the buffer and can still be read.
void closechan(struct chan* c);

// Send data on a channel, blocks if there is no room, panics if closed
bool sendchan(struct chan* c, void* ep, bool block);

// Receive data over a channel, blocks if there is no data, returns false if closed
bool recvchan(struct chan* c, void** ep, bool block, bool* selected);

// Select across multiple channel operations, choosing the first that succeeds, or none if block is set to NONBLOCK.
int selectchan(struct chan* reads[], int readlen, void** out, bool* ok, struct chan* writes[], int writelen, void* ins[], bool block);

#endif // _CHAN_H
