#ifndef _CHAN_COMMON_H
#define _CHAN_COMMON_H

#include <stdio.h>

// Macro for iterating through any linked list.
#define LLforeach(type, ptr, list) for(type ptr = list; ptr != NULL; ptr = ptr->next)

// we want to know how many crashes there were when running tests.
#ifdef NO_CRASH_ON_PANIC
extern int chan_crashed;
#endif

// panic, throw: prints a message, then segfaults
#define panic(msg) __chan_panic(msg, __FILE__, __LINE__, __PRETTY_FUNCTION__)
#define throw(msg) __chan_throw(msg, __FILE__, __LINE__, __PRETTY_FUNCTION__)
// warn: prints message about unimplemented behaviour and continues
#define warn(msg) __chan_warn(msg, __FILE__, __LINE__, __PRETTY_FUNCTION__)
static inline void __chan_panic(const char* err, const char* file, int line, const char* func)
{
	printf("panic: %s:%d:%s: %s\n", file, line, func, err);
	#ifndef NO_CRASH_ON_PANIC
	((int*)NULL)[0]++;
	#else
	chan_crashed++;
	#endif
}
static inline void __chan_throw(const char* err, const char* file, int line, const char* func)
{
	printf("fatal error: %s:%d:%s: %s\n", file, line, func, err);
	#ifndef NO_CRASH_ON_PANIC
	((int*)NULL)[0]++;
	#else
	chan_crashed++;
	#endif
}
static inline void __chan_warn(const char* err, const char* file, int line, const char* func)
{
	printf("warning: %s:%d:%s: %s\n", file, line, func, err);
}

#endif // _CHAN_COMMON_H
