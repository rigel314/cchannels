#ifndef _CHAN_G_H
#define _CHAN_G_H

#include <stdatomic.h>

#include "chan_int.h"

struct g
{
	struct g* next;
	pthread_t tid;
	pthread_cond_t cond;
	void* param;
	struct g* closelink;
	pthread_mutex_t parklock;
	struct sudog* waiting;
	_Atomic int selectDone;
};

extern struct g* gall;

struct g* getg();

void gpark();

#endif // _CHAN_G_H
