#include <pthread.h>
#include <stdlib.h>

#include "g.h"
#include "chan_common.h"

// g list, and lock for g list, nothing is ever removed from gall
// gall is automatically initialized and appended when needed by a blocking channel
pthread_rwlock_t glist_lock = PTHREAD_RWLOCK_INITIALIZER;
struct g* gall = NULL;

struct g* newg()
{
	struct g* ret = NULL;

	ret = calloc(1, sizeof(*ret));

	if(ret == NULL)
	{
		return NULL;
	}

	// ret->next = NULL; // calloc()'d
	ret->tid = pthread_self();
	// ret->param = NULL; // calloc()'d
	// ret->closelink = NULL; // calloc()'d
	if(pthread_cond_init(&ret->cond, NULL))
	{
		// PTHREAD_RETURN:
		free(ret);
		return NULL;
	}
	if(pthread_mutex_init(&ret->parklock, NULL))
	{
		// PTHREAD_RETURN:
		pthread_cond_destroy(&ret->cond);
		free(ret);
		return NULL;
	}

	return ret;
}

struct g* getg()
{
	struct g* ret = NULL;

	if(pthread_rwlock_rdlock(&glist_lock))
	{
		// PTHREAD_RETURN:
		panic("read lock acquire failed, too many read locks?");
	}

	// Walk the list looking for existing g for current thread
	LLforeach(struct g*, ptr, gall)
	{
		if(pthread_equal(ptr->tid, pthread_self()))
		{
			ret = ptr;
			break;
		}
	}

	pthread_rwlock_unlock(&glist_lock);

	// if it was found in the list
	if(ret)
	{
		return ret;
	}

	pthread_rwlock_wrlock(&glist_lock);
	ret = newg();
	if(!ret)
	{
		panic("couldn't create new g, out of memory?");
	}

	// walk the list again looking for the end
	// we have to walk it again because we released the lock
	struct g* end = NULL;
	LLforeach(struct g*, ptr, gall)
	{
		end = ptr;
	}

	if(end == NULL)
	{ // no list yet
		// initialize the list with us
		gall = ret;

		pthread_rwlock_unlock(&glist_lock);
		return ret;
	}

	end->next = ret;
	pthread_rwlock_unlock(&glist_lock);
	return ret;
}

void gpark()
{
	struct g* gp = getg();
	if(gp == NULL)
	{
		return;
	}
	pthread_mutex_lock(&gp->parklock);
	pthread_cond_wait(&gp->cond, &gp->parklock);
	pthread_mutex_unlock(&gp->parklock);
}
