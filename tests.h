#define DELAY 500000
#define LONGDELAY 1000000

#define testErr(msg) {printf("FAIL: %s:%d: %s\n", __FILE__, __LINE__, msg); fails++; break;}

#define TEST(msg, size) \
	do \
	{ \
		printf("TEST: %s\n", msg); \
		chan_crashed = 0; \
		sz = size; \
		out = NULL; \
		in = rand(); \
		ch = makechan(sz);
#define ENDTEST(...) \
		destroychan(ch); \
		printf("PASS\n"); \
	} while(0); printf("\n")

#define TESTmanual(msg) \
	do \
	{ \
		printf("TEST: %s\n", msg); \
		chan_crashed = 0; \
		out = NULL; \
		in = rand();
#define ENDTESTmanual(...) \
		printf("PASS\n"); \
	} while(0); printf("\n")

void* delayed_blocking_send(void* c);
void* delayed_blocking_recv(void* c);
void* blocking_send(void* c);
void* blocking_recv(void* c);
void* blocking_send_NULL(void* c);
void* delayed_close(void* c);

void* test_writeNull(void* c);
void* test_readNull(void* c);
void* test_selectNull(void* c);

void* rand();
void __testErr(char* msg, char* file, int line);
