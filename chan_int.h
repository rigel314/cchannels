// For internal use only

#ifndef _CHAN_INT_H
#define _CHAN_INT_H

#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "chan_common.h"
#include "g.h"

struct sudog
{
	struct g* g;
	bool isSelect;
	struct sudog* next;
	struct sudog* prev;
	void* elem;
	// struct sudog* parent;
	struct sudog* waitlink;
	// struct sudog* waittail;
	struct chan* c;
};
struct waitq
{
	struct sudog* first;
	struct sudog* last;
};

struct chan
{
	_Atomic size_t qcount;
	size_t dataqsiz;
	size_t elemsize;
	_Atomic int closed;
	unsigned int sendx;
	unsigned int recvx;
	struct waitq recvq;
	struct waitq sendq;
	pthread_mutex_t lock;
	void** buf;
};

enum caseKind { caseNull, caseRecv, caseSend, caseDefault };
struct scase
{
	void* elem;
	struct chan* c;
	enum caseKind kind;
	bool* receivedp;
};

struct selunlock_t
{
	struct scase* scases;
	uint16_t* lockorder;
	int len;
};

typedef void (unlockFunc)(void* data);

void chanunlock_func();
void sendchan_int(struct chan* c, struct sudog* sg, void* ep, unlockFunc* unlockf, void* ufdata);
void recvchan_int(struct chan* c, struct sudog* sg, void** ep, unlockFunc* unlockf, void* ufdata);
void enqueuesendq(struct waitq* q, struct sudog* sgp);
struct sudog* dequeuewaitq(struct waitq* q);
int selectchan_int(struct scase scases[], int ncase);
void dequeueSudoG(struct waitq* q, struct sudog* sgp);
void sellock(struct scase scases[], uint16_t lockorder[], int len);
void selunlock(struct scase scases[], uint16_t lockorder[], int len);
void selunlock_wrap(void* data);

#endif // _CHAN_INT_H
