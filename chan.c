#include <stdlib.h>

#include "chan.h"
#include "chan_int.h"

struct chan* makechan(int size)
{
	struct chan* c;

	if(size == 0)
	{
		c = calloc(1, sizeof(*c));
		c->buf = (void**)c;
	}
	else
	{
		// Allocate chan and buf in one call
		c = calloc(1, sizeof(*c) + (size * sizeof(&c->buf)));
		c->buf = (void**)(((char*) c) + sizeof(*c));
	}

	c->elemsize = sizeof(void*);
	c->dataqsiz = size;

	pthread_mutex_init(&c->lock, NULL);

	return c;
}

void destroychan(struct chan* c)
{
	pthread_mutex_lock(&c->lock);
	if(c->closed == 0)
	{
		pthread_mutex_unlock(&c->lock);
		closechan(c);
	}
	else
	{
		pthread_mutex_unlock(&c->lock);
	}
	pthread_mutex_destroy(&c->lock);
	free(c);
}

void closechan(struct chan* c)
{
	if(c == NULL)
	{
		panic("close of nil channel");
		return;
	}

	pthread_mutex_lock(&c->lock);
	if(c->closed != 0)
	{
		pthread_mutex_unlock(&c->lock);
		panic("close of closed channel");
		return;
	}

	c->closed = 1;

	struct g* glink = NULL;

	// release all readers
	struct sudog* sg;
	while((sg = dequeuewaitq(&c->recvq)) != NULL)
	{
		if(sg->elem != NULL)
		{
			sg->elem = NULL;
		}

		struct g* gp = sg->g;
		// mark channel closed for blocking i/o
		gp->param = NULL;

		// use existing g's to store a linked list of g's to wake up due to channel closure
		gp->closelink = glink;
		glink = gp;
	}

	// release all writers (they will panic)
	while((sg = dequeuewaitq(&c->sendq)) != NULL)
	{
		sg->elem = NULL;

		struct g* gp = sg->g;
		// mark channel closed for blocking i/o
		gp->param = NULL;

		// continue to use existing g's to store a linked list of g's to wake up due to channel closure
		gp->closelink = glink;
		glink = gp;
	}

	pthread_mutex_unlock(&c->lock);

	// Continue all g's now that we've unlocked the channel
	while(glink != NULL)
	{
		struct g* gp = glink;
		glink = glink->closelink;
		gp->closelink = NULL;
		pthread_mutex_lock(&gp->parklock);
		pthread_cond_signal(&gp->cond);
		pthread_mutex_unlock(&gp->parklock);
	}
}

bool sendchan(struct chan* c, void* ep, bool block)
{
	if(c == NULL)
	{
		if(!block)
		{
			return false;
		}
		// gopark to hang until main exits
		gpark();
		throw("unreachable");
		return false;
	}

	if(!block && atomic_load(&c->closed) == 0 && // non-blocking write and
		((c->dataqsiz == 0 && c->recvq.first == NULL) || // unbuffered and noone waiting
		(c->dataqsiz > 0 && atomic_load(&c->qcount) == c->dataqsiz))) // buffered and no space
	{
		return false;
	}

	pthread_mutex_lock(&c->lock);

	if(c->closed != 0)
	{
		pthread_mutex_unlock(&c->lock);
		panic("send on closed channel");
		return false;
	}

	struct sudog* sg = dequeuewaitq(&c->recvq);
	if(sg != NULL)
	{
		sendchan_int(c, sg, ep, chanunlock_func, &c->lock);
		return true;
	}

	if(c->qcount < c->dataqsiz)
	{
		c->buf[c->sendx] = ep;
		c->sendx++;
		if(c->sendx == c->dataqsiz)
		{
			c->sendx = 0;
		}
		c->qcount++;

		pthread_mutex_unlock(&c->lock);
		return true;
	}

	if(!block)
	{
		pthread_mutex_unlock(&c->lock);
		return false;
	}

	struct sudog* mysg = calloc(1, sizeof(struct sudog));
	mysg->elem = ep;
	mysg->waitlink = NULL;
	mysg->isSelect = false;
	mysg->c = c;
	struct g* gp = getg();
	pthread_mutex_lock(&gp->parklock);
	mysg->g = gp;
	mysg->g->waiting = mysg;
	mysg->g->param = NULL;
	enqueuesendq(&c->sendq, mysg);

	// unlock and sleep until send complete
	pthread_mutex_unlock(&c->lock);
	pthread_cond_wait(&mysg->g->cond, &gp->parklock);
	pthread_mutex_unlock(&gp->parklock);

	// check if closed and panic
	if(gp->param == NULL)
	{
		if(c->closed == 0)
		{
			throw("sendchan: spurious wakeup");
			return false;
		}
		panic("send on closed channel");
		return false;
	}
	gp->param = NULL;

	mysg->g->waiting = NULL;
	free(mysg);

	return true;
}

void chanunlock_func(void* data)
{
	pthread_mutex_unlock(data);
}

void sendchan_int(struct chan* c, struct sudog* sg, void* ep, unlockFunc* unlockf, void* ufdata)
{
	(void) c;
	if(sg->elem != NULL)
	{
		*((void**)sg->elem) = ep;
	}

	struct g* gp = sg->g;

	unlockf(ufdata);

	// flag data moved
	gp->param = sg;
	pthread_mutex_lock(&gp->parklock);
	pthread_cond_signal(&gp->cond);
	pthread_mutex_unlock(&gp->parklock);
}

bool recvchan(struct chan* c, void** ep, bool block, bool* selected)
{
	*selected = false;

	if(c == NULL)
	{
		if(!block)
		{
			return false;
		}
		// gopark to hang until main exits
		gpark();
		throw("unreachable");
		return false;
	}

	// TODO: atomic c->qcount read
	// TODO: atomic c->closed read
	if(!block && // non-blocking write and
		((c->dataqsiz == 0 && c->sendq.first == NULL) || // unbuffered and noone waiting
		(c->dataqsiz > 0 && atomic_load(&c->qcount) == 0)) && // buffered and nothing in buffer
		atomic_load(&c->closed) == 0)
	{
		return false;
	}

	*selected = true;
	pthread_mutex_lock(&c->lock);

	if(c->closed != 0 && c->qcount == 0)
	{
		pthread_mutex_unlock(&c->lock);
		if(ep != NULL)
		{
			*ep = NULL;
		}
		return false;
	}

	struct sudog* sg = dequeuewaitq(&c->sendq);
	if(sg != NULL)
	{
		recvchan_int(c, sg, ep, chanunlock_func, &c->lock);
		return true;
	}

	if(c->qcount > 0)
	{
		if(ep != NULL)
		{
			*ep = c->buf[c->recvx];
		}
		c->buf[c->recvx] = NULL;

		c->sendx++;
		if(c->sendx == c->dataqsiz)
		{
			c->sendx = 0;
		}
		c->qcount--;

		pthread_mutex_unlock(&c->lock);
		return true;
	}

	if(!block)
	{
		pthread_mutex_unlock(&c->lock);
		*selected = false;
		return false;
	}

	struct sudog* mysg = calloc(1, sizeof(struct sudog));
	mysg->elem = ep;
	mysg->waitlink = NULL;
	mysg->isSelect = false;
	mysg->c = c;
	struct g* gp = getg();
	pthread_mutex_lock(&gp->parklock);
	mysg->g = gp;
	mysg->g->waiting = mysg;
	mysg->g->param = NULL;
	enqueuesendq(&c->recvq, mysg);

	// unlock and sleep until recv complete
	pthread_mutex_unlock(&c->lock);
	pthread_cond_wait(&mysg->g->cond, &gp->parklock);
	pthread_mutex_unlock(&gp->parklock);

	// check if closed
	int closed = gp->param == NULL;
	gp->param = NULL;

	mysg->g->waiting = NULL;
	free(mysg);

	return !closed;
}

void recvchan_int(struct chan* c, struct sudog* sg, void** ep, unlockFunc* unlockf, void* ufdata)
{
	if(c->dataqsiz == 0)
	{
		if(ep != NULL)
		{
			*ep = sg->elem;
		}
	}
	else
	{
		if(ep != NULL)
		{
			*ep = c->buf[c->recvx];
		}
		c->buf[c->recvx] = sg->elem;

		c->recvx++;
		if(c->recvx == c->dataqsiz)
		{
			c->recvx = 0;
		}
		c->sendx = c->recvx;
	}

	sg->elem = NULL;

	struct g* gp = sg->g;

	unlockf(ufdata);

	// flag data moved
	gp->param = sg;
	pthread_mutex_lock(&gp->parklock);
	pthread_cond_signal(&gp->cond);
	pthread_mutex_unlock(&gp->parklock);
}

void enqueuesendq(struct waitq* q, struct sudog* sgp)
{
	sgp->next = NULL;
	struct sudog* x = q->last;
	if(x == NULL)
	{
		sgp->prev = NULL;
		q->first = sgp;
		q->last = sgp;
		return;
	}

	sgp->prev = x;
	x->next = sgp;
	q->last = sgp;
}

struct sudog* dequeuewaitq(struct waitq* q)
{
	struct sudog* sgp = NULL;
	while((sgp = q->first) != NULL)
	{
		struct sudog* y = sgp->next;
		if(y == NULL)
		{
			q->first = NULL;
			q->last = NULL;
		}
		else
		{
			y->prev = NULL;
			q->first = y;
			sgp->next = NULL;
		}

		if(sgp->isSelect)
		{
			// if a thread was put on this queue because of a
			// select, there is a small window between the thread
			// being woken up by a different case and it grabbing the
			// channel locks. Once it has the lock
			// it removes itself from the queue, so we won't see it after that.
			// We use a flag in the G struct to tell us when someone
			// else has won the race to signal this thread but the thread
			// hasn't removed itself from the queue yet.
			int expected = 0;
			if(!atomic_compare_exchange_strong(&sgp->g->selectDone, &expected /* 0 */, 1))
			{
				continue;
			}
		}

		return sgp;
	}

	return sgp;
}

int selectchan_int(struct scase scases[], int ncase)
{
	uint16_t pollorder[ncase];
	uint16_t lockorder[ncase];

	// initialize variable sized pollorder & lockorder
	for(int i = 0; i < ncase; i++)
	{
		pollorder[i] = lockorder[i] = 0;
	}

	// generate permuted order
	for(int i = 0; i < ncase; i++)
	{
		pollorder[i] = i; // MAYBE: make this random
	}

	// sort cases by chan address
	for(int i = 0; i < ncase; i++)
	{
		int j = i;

		struct chan* c = scases[pollorder[i]].c;
		while(j > 0 && (uintptr_t)(scases[lockorder[(j-1)/2]].c) < (uintptr_t)c)
		{
			int k = (j-1)/2;
			lockorder[j] = lockorder[k];
			j = k;
		}
		lockorder[j] = pollorder[i];
	}
	for(int i = ncase-1; i >= 0; i--)
	{
		int o = lockorder[i];
		struct chan* c = scases[o].c;
		lockorder[i] = lockorder[0];
		int j = 0;
		while(1)
		{
			int k = j*2 + 1;
			if(k >= i)
			{
				break;
			}
			if(k+1 < i && (uintptr_t)(scases[lockorder[k]].c) < (uintptr_t)(scases[lockorder[k+1]].c))
			{
				k++;
			}
			if((uintptr_t)c < (uintptr_t)(scases[lockorder[k]].c))
			{
				lockorder[j] = lockorder[k];
				j = k;
				continue;
			}
			break;
		}
		lockorder[j] = o;
	}
	#ifdef NO_CRASH_ON_PANIC // when I'm testing
	for(int i = 0; i < ncase; i++)
	{
		printf("scases[%d].c: %p, scases[lockorder[%d]].c: %p\n", i, scases[i].c, i, scases[lockorder[i]].c);
	}
	for(int i = 0; i+1 < ncase; i++)
	{
		if(scases[lockorder[i]].c > scases[lockorder[i+1]].c)
		{
			throw("bad sort");
		}
	}
	#endif

	// lock all channels involved
	sellock(scases, lockorder, ncase);

	struct g* gp = NULL;
	struct sudog* sg = NULL;
	struct chan* c = NULL;
	struct scase* k = NULL;
	struct sudog* sglist = NULL;
	struct sudog* sgnext = NULL;
	// void* qp = NULL;
	struct sudog** nextp = NULL;

	int dfli;
	struct scase* dfl;
	int casi;
	struct scase* cas;

loop:
	// pass 1 - look for something already waiting
	dfli = 0;
	dfl = NULL;
	// casi = 0; // will always be set before read
	cas = NULL;
	for(int i = 0; i < ncase; i++)
	{
		casi = pollorder[i];
		cas = &scases[casi];
		c = cas->c;

		switch(cas->kind)
		{
			case caseNull:
				continue;

			case caseRecv:
				sg = dequeuewaitq(&c->sendq);
				if(sg != NULL)
				{
					goto recv;
				}
				if(c->qcount > 0)
				{
					goto bufrecv;
				}
				if(c->closed != 0)
				{
					goto rclose;
				}
				break;

			case caseSend:
				if(c->closed != 0)
				{
					goto sclose;
				}
				sg = dequeuewaitq(&c->recvq);
				if(sg != NULL)
				{
					goto send;
				}
				if(c->qcount < c->dataqsiz)
				{
					goto bufsend;
				}
				break;

			case caseDefault:
				dfli = casi;
				dfl = cas;
				break;
		}
	}
	if(dfl != NULL)
	{
		selunlock(scases, lockorder, ncase);
		casi = dfli;
		// cas = dfl; // cas only used for releasetime
		goto retc;
	}

	// pass 2 - enqueue on all chans
	gp = getg();
	pthread_mutex_lock(&gp->parklock);
	if(gp->waiting != NULL)
	{
		throw("gp.waiting != NULL");
		return -1;
	}
	nextp = &gp->waiting;
	for(int i = 0; i < ncase; i++)
	{
		int casei = lockorder[i];
		casi = casei;
		cas = &scases[casi];
		if(cas->kind == caseNull)
		{
			continue;
		}
		c = cas->c;
		struct sudog* sg = calloc(1, sizeof(struct sudog));
		sg->g = gp;
		sg->isSelect = true;
		sg->elem = cas->elem;
		sg->c = c;

		// Construct waiting list in lock order
		*nextp = sg;
		nextp = &sg->waitlink;

		switch(cas->kind)
		{
			case caseRecv:
				enqueuesendq(&c->recvq, sg);
				break;

			case caseSend:
				enqueuesendq(&c->sendq, sg);
				break;

			default:
				break;
		}
	}

	// wait for someone to wake us up
	gp->param = NULL;
	// This should be fine because of the comment on sellock below.
	selunlock(scases, lockorder, ncase);
	pthread_cond_wait(&gp->cond, &gp->parklock);
	pthread_mutex_unlock(&gp->parklock);

	// gp->waiting will be null if the select only consists of NULL channels.
	// Of course, in that case, nothing would ever wake this thread, but
	// scan-build doesn't know that.
	if(gp->waiting == NULL)
	{
		throw("unreachable"); // for scan-build
		return -1;
	}

	// waking up from sleep, then relocking the channels isn't a race condition
	// because when an sg is dequeued, its g is atomically checked for selectDone
	sellock(scases, lockorder, ncase);

	gp->selectDone = 0; // We don't need an atomic store because nothing can be setting this while we have all the channels locked.
	// atomic_store(&gp->selectDone, 0);
	sg = (struct sudog*)gp->param;
	gp->param = NULL;

	// pass 3 - dequeue from unsuccessful chans
	casi = -1;
	cas = NULL;
	sglist = gp->waiting;

	for(struct sudog* sg1 = gp->waiting; sg1 != NULL; sg1 = sg1->waitlink)
	{
		sg1->isSelect = false;
		sg1->elem = NULL;
		sg1->c = NULL;
	}
	gp->waiting = NULL;

	for(int i = 0; i < ncase; i++)
	{
		int casei = lockorder[i];
		k = &scases[casei];
		if(k->kind == caseNull)
		{
			continue;
		}
		if(sg == sglist)
		{
			// sg has already been dequeued by the thread that woke us up
			casi = casei;
			cas = k;
		}
		else
		{
			c = k->c;
			if(k->kind == caseSend)
			{
				dequeueSudoG(&c->sendq, sglist);
			}
			else
			{
				dequeueSudoG(&c->recvq, sglist);
			}
		}
		sgnext = sglist->waitlink;
		sglist->waitlink = NULL;
		free(sglist);
		sglist = sgnext;
	}

	if(cas == NULL)
	{
		// If we wake up due to channel closure, this will happen
		// It's easiest if we just go back to the top to detect whether it was a read or write close
		goto loop;
	}

	// c = cas->c; // only exists for msan and debug

	if(cas->kind == caseRecv && cas->receivedp != NULL)
	{
		*cas->receivedp = true;
	}

	selunlock(scases, lockorder, ncase);
	goto retc;

bufrecv:
	// can receive from buffer
	if(cas->receivedp != NULL)
	{
		*cas->receivedp = true;
	}
	if(cas->elem != NULL)
	{
		*((void**)cas->elem) = c->buf[c->recvx];
	}
	c->buf[c->recvx] = NULL;
	c->recvx++;
	if(c->recvx == c->dataqsiz)
	{
		c->recvx = 0;
	}
	c->qcount--;
	selunlock(scases, lockorder, ncase);
	goto retc;

bufsend:
	// can send to buffer
	c->buf[c->sendx] = cas->elem;
	c->sendx++;
	if(c->sendx == c->dataqsiz)
	{
		c->sendx = 0;
	}
	c->qcount++;
	selunlock(scases, lockorder, ncase);

	// DIFFERENCE: golang writes don't set cas->receivedp
	if(cas->receivedp != NULL)
	{
		*cas->receivedp = true;
	}
	goto retc;

recv:
	// can receive from sleeping sender (sg)
	(void) (2+2); // Yes, C, I know I can't jump to a variable declaration
	struct selunlock_t su = {scases, lockorder, ncase};
	recvchan_int(c, sg, cas->elem, selunlock_wrap, &su);
	if(cas->receivedp != NULL)
	{
		*cas->receivedp = true;
	}
	goto retc;

rclose:
	// read at end of closed channel
	selunlock(scases, lockorder, ncase);
	if(cas->receivedp != NULL)
	{
		*cas->receivedp = false;
	}
	if(cas->elem != NULL)
	{
		*((void**)cas->elem) = NULL;
	}
	goto retc;

send:
	// can send to a sleeping receiver (sg)
	(void) (2+2); // Yes, C, I know I can't jump to a variable declaration
	struct selunlock_t su2 = {scases, lockorder, ncase};
	sendchan_int(c, sg, cas->elem, selunlock_wrap, &su2);

	// DIFFERENCE: golang writes don't set cas->receivedp
	if(cas->receivedp != NULL)
	{
		*cas->receivedp = true;
	}
	goto retc;

retc:
	return casi;

sclose:
	selunlock(scases, lockorder, ncase);
	panic("send on closed channel");
	return -1;
}

void dequeueSudoG(struct waitq* q, struct sudog* sgp)
{
	struct sudog* x = sgp->prev;
	struct sudog* y = sgp->next;

	if(x != NULL)
	{
		if(y != NULL)
		{
			// middle of queue
			x->next = y;
			y->prev = x;
			sgp->next = NULL;
			sgp->prev = NULL;
			return;
		}
		// end of queue
		x->next = NULL;
		q->last = x;
		sgp->prev = NULL;
		return;
	}

	if(y != NULL)
	{
		// start of queue
		y->prev = NULL;
		q->first = y;
		sgp->next = NULL;
		return;
	}

	// x == y == NULL.  Either sgp is the only element or it has already been removed.
	// Use q->first to disambiguate.
	if(q->first == sgp)
	{
		q->first = NULL;
		q->last = NULL;
	}
}

int selectchan(struct chan* reads[], int readlen, void** out, bool* ok, struct chan* writes[], int writelen, void* ins[], bool block)
{
	struct scase cases[readlen + writelen + 1];
	void* outs[readlen];
	bool oks[readlen+writelen];

	for(int i = 0; i < readlen + writelen; i++)
	{
		oks[i] = false; // initialize oks
		if(i < readlen)
		{
			outs[i] = NULL;
		}

		cases[i].receivedp = &oks[i];
		cases[i].elem = (i < readlen) ? &outs[i] : ins[i-readlen];
		cases[i].c = (i < readlen) ? reads[i] : writes[i-readlen];
		cases[i].kind = (i < readlen) ? caseRecv : caseSend;
		if(cases[i].c == NULL)
		{
			cases[i].kind = caseNull;
		}
	}

	if(!block)
	{
		int i = readlen + writelen;
		cases[i].elem = NULL;
		cases[i].receivedp = NULL;
		cases[i].c = NULL;
		cases[i].kind = caseDefault;
	}

	int ret = selectchan_int(cases, readlen + writelen + !block);

	void* tmpout = NULL;
	bool tmpok = false;

	if(ret >= 0 && ret < readlen)
	{
		tmpout = outs[ret];
	}
	if(ret >= 0 && ret < readlen + writelen)
	{
		tmpok = oks[ret];
	}

	if(out != NULL)
	{
		*out = tmpout;
	}
	if(ok != NULL)
	{
		*ok = tmpok;
	}

	return ret;
}

void sellock(struct scase scases[], uint16_t lockorder[], int len)
{
	struct chan* prev = NULL;
	for(int i = 0; i < len; i++)
	{
		int o = lockorder[i];
		struct chan* ch = scases[o].c;
		if(ch != NULL && ch != prev)
		{
			prev = ch;
			pthread_mutex_lock(&ch->lock);
		}
	}
}

void selunlock(struct scase scases[], uint16_t lockorder[], int len)
{
	// Unlock in reverse lock order
	for(int i = len - 1; i >= 0; i--)
	{
		struct chan* ch = scases[lockorder[i]].c;
		if(ch == NULL)
		{
			break; // We're done because they are sorted and all the rest must also be NULL
		}
		if(i > 0 && ch == scases[lockorder[i-1]].c)
		{
			continue; // We'll unlock this one next iteration
		}
		pthread_mutex_unlock(&ch->lock);
	}
}

void selunlock_wrap(void* data)
{
	struct selunlock_t* su = data;
	selunlock(su->scases, su->lockorder, su->len);
}

#ifdef NO_CRASH_ON_PANIC
int chan_crashed = 0;
#endif
